# -*- coding: utf-8 -*-
from flask import Flask, request, jsonify, Response

app = Flask(__name__)
app.config.from_pyfile('../config.py', silent=True)

@app.route("/admin/status", methods=['GET'])
def get_status():
  """
  This function is a endpoint status which return application [name/version] and current environment
  :return: json object
  """
  return jsonify(application_name=app.config['APP_NAME'],
                 application_version=app.config['VERSION'],
                 environment=app.config['ENVIRONMENT'])

@app.errorhandler(404)
def error_404(error):
  """
  :param error: http exception
  :return: 404 error page
  """
  return "404 NOT FOUND", 404

if __name__ == "__main__":
  app.run(host=app.config['HOST'], port=app.config['PORT'])