# -*- coding: utf-8 -*-
from setuptools import setup, find_packages

with open('README.md') as file:
    readme = file.read()

with open('LICENSE') as file:
    licence = file.read()

setup (
    name='neo-api',
    version='0.1.0',
    description='Python API package for IRC demonstration',
    environment='dev',
    long_description=readme,
    author='Victor Ladouceur',
    author_email='victor.ladouceur@gmail.com',
    url='https://gitlab.com/microservice-inside/neo-api',
    license=license,
    packages=find_packages(),
    classifiers=[
        "Programming Language :: Python",
        "Development Status :: 1 - Planning",
        "License :: OSI Approved",
        "Operating System :: OS Independent",
        "Programming Language :: Python :: 3.7.2",
    ]
)