# Python Flask featuring Gitlab 

[![pipeline status](https://gitlab.com/microservice-inside/java-gitlab-integration/badges/master/pipeline.svg)](https://gitlab.com/microservice-inside/java-gitlab-integration/commits/master)

- Python version: 3.7.2
- Flask version: 1.0.2
- Docker engine version: 18.09.2

---

## Features

- Python Flask application which exposes some REST endpoints
- Gitlab pipeline which demonstrates how to implements a simple continuous integration workflow

## Requirements

- Then you'll need Java on your local machine if you haven't it yet [Python] (https://www.python.org/)
- And also Flask [Flask installation] (http://flask.pocoo.org/)