# -*- coding: utf-8 -*-

APP_NAME = 'neo_api'
ENVIRONMENT = 'int'
VERSION = '0.1.0'
HOST = '0.0.0.0'
PORT = 80
DEBUG = True
TESTING = True